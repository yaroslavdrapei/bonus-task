﻿call cnt_of_books_borrowed_by_customer(6, @count);
select @count;
call cnt_of_books_borrowed_by_customer(7, @count);
select @count;
call cnt_of_books_borrowed_by_customer(8, @count);
select @count;

set @genreId = 5;
call get_cnt_authors_genre(@genreId);
select @genreId;

set @genreId = 6;
call get_cnt_authors_genre(@genreId);
select @genreId;

set @genreId = 1;
call get_cnt_authors_genre(@genreId);
select @genreId;

select * from authors;
select * from book_author where author_id = 8;

set autocommit = 0;

set autocommit = 1;

call deletion_of_author(34);
call deletion_of_author(8);