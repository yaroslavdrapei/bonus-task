﻿DELIMITER //

CREATE PROCEDURE cnt_of_books_borrowed_by_customer_proc(IN id INT, OUT counter INT)
BEGIN
    SELECT COUNT(l.customer_id) AS books_borrowed INTO counter FROM loans l
    INNER JOIN customers c ON l.customer_id = c.customer_id
    WHERE l.customer_id = id
    GROUP BY l.customer_id;

    IF counter IS NULL THEN
        SET counter = 0;
    END IF;
END //

DELIMITER ;

DELIMITER //

CREATE PROCEDURE get_cnt_authors_genre_proc(INOUT genre INT)
BEGIN
    DECLARE counter INT;

    SELECT COUNT(ba.author_id) INTO counter FROM genres g
    INNER JOIN book_genre bg ON g.genre_id = bg.genre_id
    INNER JOIN books ON bg.isbn = books.isbn
    INNER JOIN book_author ba ON books.book_id = ba.book_id
    WHERE g.genre_id = genre
    GROUP BY g.genre_id;

    IF counter IS NULL THEN
        SET genre = 0;
    ELSE
        SET genre = counter;
    END IF;
END //

DELIMITER ;

SET autocommit = 0;
DELIMITER //

CREATE PROCEDURE deletion_of_author_proc(IN id INT)
BEGIN
    DECLARE should_commit BOOLEAN DEFAULT TRUE;
    START TRANSACTION;

    DELETE FROM book_author
    WHERE author_id = id;

    IF ROW_COUNT() > 0 THEN
        DELETE FROM authors 
        WHERE author_id = id;
        IF ROW_COUNT() > 0 THEN
            COMMIT;
        ELSE
            SET should_commit = FALSE;
        END IF;
    ELSE
        SET should_commit = FALSE;
    END IF;

    IF NOT should_commit THEN
        ROLLBACK ;
    END IF;
END //

DELIMITER ;

SET autocommit = 1;
