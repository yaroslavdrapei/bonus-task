﻿create table Book_Author (
    book_id int,
    author_id int,
    foreign key (book_id) references books(book_id),
    foreign key (author_id) references authors(author_id)
);

alter table Book_Author 
add primary key (book_id, author_id);

INSERT INTO Book_Author (book_id, author_id) VALUES
 (12, 5),
 (25, 14),
 (6, 29),
 (18, 10),
 (3, 17),
 (30, 21),
 (9, 3),
 (2, 8),
 (23, 11),
 (16, 19),
 (29, 22),
 (7, 28),
 (14, 1),
 (21, 26),
 (11, 7),
 (4, 13),
 (19, 18),
 (5, 15),
 (26, 24),
 (20, 16),
 (13, 2),
 (28, 12),
 (15, 30),
 (22, 27),
 (10, 20),
 (1, 6),
 (8, 23),
 (27, 9),
 (17, 25),
 (24, 4),
 (30, 10),
 (20, 22),
 (14, 29),
 (5, 19),
 (11, 26),
 (3, 7),
 (21, 16),
 (28, 12),
 (9, 1),
 (19, 25);

alter table Book_Author
add foreign key (book_id) references books(book_id);

alter table books
modify column book_id int;

alter table books
drop primary key;

alter table books
add primary key (book_id, isbn);

alter table Book_Author
add foreign key (book_id) references books(book_id);

select * from books;

create table Book_Genre (
    isbn varchar(255),
    genre_id int,
    foreign key (genre_id) references genres(genre_id)
);

alter table Book_Genre
add primary key (isbn, genre_id);

alter table books
add index (isbn);

alter table Book_Genre
add foreign key (isbn) references books(isbn);

INSERT INTO Book_Genre (isbn, genre_id) VALUES
('9780590353427', 3),
('9780743273565', 4),
('9780061120084', 2),
('9780451524935', 1),
('9780141439518', 5),
('9780316769488', 1),
('9780547928227', 3),
('9780140283334', 2),
('9780060850524', 4),
('9780142437247', 5),
('9780307474278', 1),
('9780007117307', 2),
('9780061122415', 3),
('9780261103573', 4),
('9781853260391', 5),
('9781416548942', 1),
('9780345391803', 2),
('9780141439556', 3),
('9780141439648', 4),
('9780142437209', 5),
('9780141439570', 1),
('9780307265432', 2),
('9780684800714', 3),
('9780064401883', 4),
('9780141439747', 5),
('9780140449266', 1),
('9780141439846', 2),
('9780141439471', 3),
('9780141439761', 4),
('9780451419439', 5),
('9780590353427', 3),
('9780743273565', 4),
('9780061120084', 2),
('9780451524935', 1),
('9780141439518', 5),
('9780316769488', 1),
('9780547928227', 3),
('9780140283334', 2),
('9780060850524', 4),
('9780142437247', 5),
('9780307474278', 1),
('9780007117307', 2),
('9780061122415', 3),
('9780261103573', 4),
('9781853260391', 5),
('9781416548942', 1),
('9780345391803', 2),
('9780141439556', 3),
('9780141439648', 4);

select customers.customer_id, customers.name, count(loans.loan_id) from loans
inner join customers on loans.customer_id = customers.customer_id
group by customers.customer_id;

create table Book_Loan (
    book_id int,
    loan_id int,
    foreign key (book_id) references books(book_id),
    foreign key (loan_id) references loans(loan_id)
);
    
INSERT INTO Book_Loan (book_id, loan_id) VALUES
(12, 5),
(25, 14),
(6, 29),
(18, 10),
(3, 17),
(30, 21),
(9, 3),
(2, 8),
(23, 11),
(16, 19),
(29, 22),
(7, 28),
(14, 1),
(21, 26),
(11, 7),
(4, 13),
(19, 18),
(5, 15),
(26, 24),
(20, 16),
(13, 2),
(28, 12),
(15, 12),
(22, 27),
(10, 20),
(1, 6),
(8, 23),
(27, 9),
(17, 25),
(24, 4),
(30, 10),
(20, 22),
(14, 29),
(5, 19),
(11, 26),
(3, 7),
(21, 16),
(28, 12),
(9, 1),
(19, 25),
(12, 3),
(25, 14),
(6, 29),
(18, 10),
(3, 17),
(30, 21),
(9, 3),
(2, 8),
(23, 11),
(16, 19);