﻿-- todo: change names (Book_Loan => book_loan eg) done
use assignment1;

# rename table Book_Author to book_author;
# rename table Book_Genre to book_genre;
# rename table Book_Loan to book_loan;

drop table Book_Loan;
drop table Book_Author;
drop table Book_Genre;

create table book_loan
(
    book_id int,
    loan_id int,
    foreign key (book_id) references books (book_id),
    foreign key (loan_id) references loans (loan_id),
    primary key (book_id, loan_id)
);

create table book_author
(
    book_id   int,
    author_id int,
    foreign key (book_id) references books (book_id),
    foreign key (author_id) references authors (author_id),
    primary key (book_id, author_id)
);


create table book_genre
(
    isbn     varchar(255),
    genre_id int,
    foreign key (isbn) references books (isbn),
    foreign key (genre_id) references genres (genre_id),
    primary key (isbn, genre_id)
);

-- todo: remove unnecessary one-to-many done

alter table books
    drop foreign key fdf,
    drop foreign key fk_genres;
    
alter table books
drop column genre_id,
drop column author_id;

    
-- todo: add primary to book_loan

drop table Book_Loan;
create table book_loan
(
    book_id int,
    loan_id int,
    foreign key (book_id) references books (book_id),
    foreign key (loan_id) references loans (loan_id),
    primary key (book_id, loan_id)
);
