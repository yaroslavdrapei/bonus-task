﻿# how many times each book has been borrowed
select b.title, counter from books b
inner join (select book_id, count(loan_id) as counter from book_loan group by book_id) as book_counter
on b.book_id = book_counter.book_id;


# top 3 authors whose books are borrowed the most
select a.name, sum(book_counter.counter) as num_of_books_borrowed from authors a
inner join book_author ba on a.author_id = ba.author_id
inner join (select book_id, count(loan_id) as counter from book_loan group by book_id) as book_counter
on ba.book_id = book_counter.book_id
group by a.name
order by num_of_books_borrowed desc
limit 3;