﻿use tests;

create table publishers
(
    publisher_id int primary key auto_increment,
    name varchar(255),
    address varchar(255),
    phone  varchar(255),
    website varchar(255)
);

create table books
(
    book_id int primary key auto_increment,
    title varchar(255),
    isbn  varchar(255),
    edition varchar(255),
    year int,
    price float(10, 2),
    publisher_id int,
    foreign key (publisher_id) references publishers (publisher_id)
);

create table authors
(
    author_id int primary key auto_increment,
    name varchar(255),
    birthday date,
    nationality  varchar(255),
    biography text
);

create table genres
(
    genre_id int primary key auto_increment,
    name varchar(255),
    description varchar(255)
);

create table customers
(
    customer_id int primary key auto_increment,
    name varchar(255),
    address varchar(255),
    phone varchar(255),
    email varchar(255),
    password varchar(255)
);

create table loans
(
    loan_id int primary key auto_increment,
    customer_id int,
    book varchar(255),
    loan_date  date,
    due_date date,
    return_date date,
    foreign key (customer_id) references customers (customer_id)
);