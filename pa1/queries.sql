﻿use assignment1;

# most popular genre among loan books
select g.name, count(g.name) maxx from genres g 
inner join books s on s.genre_id = g.genre_id
inner join loans l on s.book_id = l.book_id
group by g.name
order by maxx desc
limit 1;

# how many books each customer borrowed
select c.name, count(l.customer_id) books_borrowed from loans l
inner join customers c on l.customer_id = c.customer_id
group by l.customer_id
order by books_borrowed desc;

# how many books each author has
select a.name, count(b.book_id) number_of_books from authors a 
inner join books b on a.author_id = b.author_id
group by a.name
order by number_of_books desc;