﻿use assignment1;

alter table books
add column author_id int;

alter table books
add constraint fdf
foreign key (author_id) references authors(author_id);

select b.title, a.name from books b
inner join authors a on b.author_id = a.author_id;
select * from authors;
select * from books;

# ------------------- genres link -------------------------
alter table books
    add column genre_id int;

alter table books
add constraint fk_genres
foreign key (genre_id) references genres(genre_id);

select * from genres;

update books b
set b.genre_id = 9
where b.book_id in (3, 15, 16, 30);

# ------------------- loans link -------------------------
select * from loans;
# delete from loans l
# where l.book not in (select b.title from books b);

# alter table loans
# change books book varchar(255)
alter table loans
add column customer_id int;

alter table loans
    add constraint fk_customer_id
        foreign key (customer_id) references customers(customer_id);

update loans l 
set l.customer_id = FLOOR(1 + RAND() * (30 - 1 + 1));
select * from customers;

# ------------------- loans to books link -------------------------
alter table loans
    add column book_id int;

alter table loans
    add constraint fk_book_id
        foreign key (book_id) references books(book_id);

update loans l
inner join books b on l.book = b.title
set l.book_id = b.book_id
where l.book = b.title;

# ------------------- publisher link -------------------------

select * from publishers;
select * from books;

alter table books
    add column publisher_id int;

alter table books
    add constraint fk_publisher_id
        foreign key (publisher_id) references publishers(publisher_id);

update books b
set b.publisher_id = FLOOR(1 + RAND() * (30 - 1 + 1));