﻿import argparse
import mysql.connector

def print_data(data: list) -> None:
  for record in data:
    print(record)

def connect_to_db(host, user, password, database=''):
  try: 
    db = mysql.connector.connect(
      host=host,
      user=user,
      password=password,
      database=database
    )
  except mysql.connector.errors.DatabaseError:
    print('Can\'t connect to MySQL server')
    exit()

  return db

parser = argparse.ArgumentParser()
parser.add_argument('host', help="Receives host")
parser.add_argument('user', help="Receives user")
parser.add_argument('password', help="Receives password")
parser.add_argument('--schema', help="Receives schema name", default='')
parser.add_argument('-s', '--select', help="Insert a query here")
parser.add_argument('-u', '--update', help="Insert a query here")
parser.add_argument('-d', '--delete', help="Insert a query here")
parser.add_argument('-c', '--create', help="Insert a query here")
args = parser.parse_args()
config = vars(args)

db = connect_to_db(config['host'], config['user'], config['password'], config['schema'])

cursor = db.cursor()

if config['select']:
  cursor.execute(config['select'])
  print_data(cursor.fetchall())
elif config['update']:
  cursor.execute(config['update'])
  db.commit()
elif config['delete']:
  cursor.execute(config['delete'])
  db.commit()
elif config['create']:
  cursor.execute(config['create'])

"""
Queries:
create: python main.py localhost root password --schema mydb -c "create table test_table (id int)"
read: python main.py localhost root password --schema mydb -s "select * from books"
update: python main.py localhost root password --schema mydb -u "update books set price = 100 where book_id < 5"
delete: python main.py localhost root password --schema mydb -d "delete from books where book_id > 10"
"""
