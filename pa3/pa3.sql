﻿select b.book_id, b.title, p.website from publishers p
inner join books b on p.publisher_id = b.publisher_id
where b.book_id = 34;

-- = NON-CORRELATED --------------------------------------------------------
    
-- select the title of the 3rd most expensive book
select b.book_id, b.title from books b
where b.price = (select b.price from books b
                 order by b.price desc
                 limit 1 offset 2);

-- give a discount for 3rd most expensive book
update books b
set b.price = b.price*0.8
where b.price = (select b.price from books b
                 order by b.price desc
                 limit 1 offset 2);

-- delete the 3rd most expensive book
delete from books b
where b.price = (select b.price from books b
                 order by b.price desc
                 limit 1 offset 2);

-- = CORRELATED -------------------------------------------------------------
    
-- selecting customer's names if he they have exactly 2 loans (counts separately for every customer)
select c.customer_id, c.name from customers c
where (select count(l.loan_id) from loans l where l.customer_id = c.customer_id group by l.customer_id) = 2;

-- changing name if ...
update customers c
set c.name = concat(c.name, ' (Has loaned 2 )')
where (select count(l.loan_id) from loans l where l.customer_id = c.customer_id group by l.customer_id) = 2;

-- deleting customers if they have exactly 2 loans
delete from customers c
where (select count(l.loan_id) from loans l where l.customer_id = c.customer_id group by l.customer_id) = 2;

-- IN NON-CORRELATED --------------------------------------------------------
    
-- Books that are in loan (non correlated)
select b.title from books b 
where b.book_id in (select bl.book_id from book_loan bl);

-- Give a discount for books in loan
update books b
set b.price = b.price*0.95
where b.book_id in (select bl.book_id from book_loan bl);

-- Delete books that are in loan
delete from books b
where b.book_id in (select bl.book_id from book_loan bl);

-- NOT IN NON-CORRELATED --------------------------------------------------------
    
-- select author names who have written less than 3 books
select a.name from authors a
where a.author_id NOT IN (select ba.author_id from book_author ba
                          group by ba.author_id
                          having count(ba.book_id) > 2);

-- give extra piece of information
update authors a
set a.biography = a.biography + ' We have less than 3 of their books'
where a.author_id NOT IN (select ba.author_id from book_author ba
                          group by ba.author_id
                          having count(ba.book_id) > 2);

-- delete all authors that have less than 3 books
delete from authors a
where a.author_id NOT IN (select ba.author_id from book_author ba
    group by ba.author_id
    having count(ba.book_id) > 2);

-- NOT EXISTS CORRELATED --------------------------------------------------------
    
-- select name of publisher that released 0 books
select p.name from publishers p
where not exists (select b.book_id from books b where b.publisher_id = p.publisher_id);

update publishers p 
set p.name = concat(p.name, ' (haven\'t published a single book yet)')
where not exists (select b.book_id from books b where b.publisher_id = p.publisher_id);

-- delete publisher that released 0 books
delete from publishers p
where not exists (select b.book_id from books b where b.publisher_id = p.publisher_id);

-- EXISTS CORRELATED --------------------------------------------------------

-- selecting genres which we have
select g.genre_id, g.name from genres g 
where exists(select bg.genre_id from book_genre bg where g.genre_id = bg.genre_id);

-- updating description
update genres g 
set g.description = concat('(We don\'t have this genre yet)', g.description)
where exists(select bg.genre_id from book_genre bg where g.genre_id = bg.genre_id);

-- deleting genres that we don't have
delete from genres g
where exists(select bg.genre_id from book_genre bg where g.genre_id = bg.genre_id);

-- UNION ALL --------------------------------------------------------
-- getting contact information of publishers and customers
select p.name, p.phone, p.address from publishers p
union all
select c.name, c.phone, c.address from customers c;

-- UNION --------------------------------------------------------
-- getting unique address of publishers and customers
select p.address from publishers p
union
select c.address from customers c;

-- INTERSECT --------------------------------------------------------
-- get books that are genre 1 and 3 at the same time

select b.title from books b 
inner join book_genre bg on b.isbn = bg.isbn
where bg.genre_id = 1
intersect
select b.title from books b
inner join book_genre bg on b.isbn = bg.isbn
where bg.genre_id = 3;

-- EXCEPT -------------------------------------------------------- 
-- getting all address of customers who are not publishers
select c.address from customers c
except
select p.address from publishers p;
